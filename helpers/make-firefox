#!/bin/bash
#
#    Copyright (C) 2008-2012  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=27

. ./config

rm debian/control

# revert https://bug851702.bugzilla.mozilla.org/attachment.cgi?id=733785
patch -p1 -R < $DATA/enable-js-options.patch

# js settings
cat $DATA/settings.js >> debian/vendor-firefox.js

# Make ubufox mandatory
#sed 's/Depends: lsb-release,/Depends: lsb-release, xul-ext-ubufox,/' -i debian/control.in
#sed 's/iceweasel,/iceweasel, firefox, icecat,/' -i debian/control.in


sed '/mozilla.org\/legal/d' -i services/healthreport/healthreport-prefs.js
cat << EOF >>services/healthreport/healthreport-prefs.js
pref("datareporting.healthreport.infoURL", "https://trisquel.info/legal");
EOF

sed 's%https://www.mozilla.org/legal/privacy/%https://trisquel.info/legal%' -i ./modules/libpref/src/init/all.js ./browser/app/profile/firefox.js ./browser/base/content/aboutDialog.xul ./toolkit/content/aboutRights.xhtml

#sed 's%https://www.mozilla.org/firefox/central/%https://trisquel.info/browser%' -i browser/base/content/browser-appmenu.inc

# Enable gst support
[ $REVISION = "6.0" ] && apt-get install -y --force-yes libgstreamermm-0.10-dev
[ $REVISION = "6.0" ] && echo "ac_add_options --enable-gstreamer=0.10" >> debian/config/mozconfig.in
[ $REVISION = "7.0" ] && echo "ac_add_options --enable-gstreamer=1.0" >> debian/config/mozconfig.in
sed 's/1204/60/g; s/1210/65/g; s/1404/70/g' -i debian/config/mozconfig.in debian/firefox-dev.install.in debian/firefox-dev.links.in

sed 's/com.ubuntu/org.trisquel/' -i debian/config/mozconfig.in

# Locale packages should provide firefox-locale-$LANG
sed "s/Provides.*/Provides: firefox-locale-@LANGCODE@/" -i debian/control.langpacks

# Remove Ubuntu bookmarks
sed -i /ubuntu-bookmarks/d debian/patches/series
rm debian/patches/ubuntu-bookmarks*

#Unbrand url codes for google and amazon
find debian/searchplugins |grep google| xargs -i /bin/sed '/ubuntu/d; /channel/d' -i {}
find debian/searchplugins |grep duck| xargs -i /bin/sed 's/canonical/trisquel/' -i {}
find debian/searchplugins |grep amazon| xargs -i /bin/sed '/canoniccom/d;' -i {}

replace "mozilla.com/plugincheck" "trisquel.info/browser" .

# contact link
#sed 's_https://input.mozilla.org/feedback_https://trisquel.info/contact_' -i browser/base/content/utilityOverlay.js

cat << EOF > debian/distribution.ini
[Global]
id=trisquel
version=$REVISION
about=Abrowser for Trisquel GNU/Linux

[Preferences]
app.distributor = "trisquel"
app.distributor.channel = "trisquel"
app.partner.ubuntu = "trisquel"
EOF

sed  "s/^MOZ_APP_NAME\t.*/MOZ_APP_NAME\t\t:= abrowser/;" debian/build/config.mk -i
sed  "s/^MOZ_PKG_NAME\t.*/MOZ_PKG_NAME\t\t:= abrowser/;" debian/build/config.mk -i

############################################################################3
############################################################################3
############################################################################3
sed "s_^Maintainer.*_Maintainer: $DEBFULLNAME <$DEBEMAIL>_g" -i debian/control.in

# Replace Firefox branding
find -type d | grep firefox | xargs rename s/firefox/abrowser/
find -type f | grep firefox | xargs rename s/firefox/abrowser/
find -type f | grep Firefox | xargs rename s/Firefox/Abrowser/
replace(){
find $3 -type f |grep -v changelog |grep -v copyright | xargs -i sed -i s^"$1"^"$2"^g "{}"
}
replace "Mozilla Firefox" "Abrowser" .
replace firefox abrowser .
replace Firefox Abrowser .
replace FIREFOX ABROWSER .
replace " Mozilla " " Trisquel " .
sed -i '2s/^Source:.*/Source: firefox/' debian/control.in
replace PACKAGES/abrowser PACKAGES/firefox .
sed s/Trisquel/Mozilla/ python/compare-locales/scripts/compare-locales -i
sed s/Trisquel/Mozilla/ python/compare-locales/setup.py -i
replace "iceweasel, abrowser" "iceweasel, firefox" .
replace "Replaces: abrowser" "Replaces: firefox" .
#sed s/Ubuntu/Trisquel/g debian/rules -i
sed s/ubuntu/trisquel/g debian/distribution.ini -i
sed 's/ubuntu_version/trisquel_version/; s/Ubuntu 10.10/Trisquel 4.0/; s/1010/40/' -i debian/abrowser.postinst.in
replace "Adobe Flash" "Flash" .

# abrowser-dev should provide firefox-dev
sed '/Package: @MOZ_PKG_NAME@-dev/,/Description:/ s/Provides:/Provides:firefox-dev, /' debian/control.in -i

# Branding files
rm browser/branding/* -rf
cp -a $DATA/branding/ browser/branding/official
cat << EOF > debian/config/branch.mk
CHANNEL                 = release
MOZ_WANT_UNIT_TESTS     = 0
# MOZ_BUILD_OFFICIAL    = 1
MOZ_ENABLE_BREAKPAD     = 0

MOZILLA_REPO = http://hg.mozilla.org/releases/mozilla-release
L10N_REPO = http://hg.mozilla.org/releases/l10n/mozilla-release
EOF

# Replace about:home
rm browser/base/content/abouthome -rf
cp $DATA/abouthome -a browser/base/content
sed '/mozilla.*png/d' -i ./browser/base/jar.mn

# Delete stuff we don't use and that may contain trademaked logos
rm -rf ./browser/metro ./mobile ./addon-sdk/source/doc/static-files/media ./browser/themes/windows ./browser/themes/osx ./b2b

#Trisquel custom bookmarks
cp $DATA/bookmarks.html.in browser/locales/generic/profile/bookmarks.html.in

mkdir debian/search
# Add DDG to search plugins
cat << EOF > debian/search/duckduckgo.xml
<SearchPlugin xmlns="http://www.mozilla.org/2006/browser/search/" xmlns:os="http://a9.com/-/spec/opensearch/1.1/">
<os:ShortName>DuckDuckGo</os:ShortName>
<os:Description>Search DuckDuckGo</os:Description>
<os:InputEncoding>UTF-8</os:InputEncoding>
<os:Image width="16" height="16">data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAAAANcNAADXDQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJyDsJmlk8pf6+v3s/v7+++zr/fcnIOyzJyDsgCcg7CYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnIOwBJyDscCcg7PZttJ7/7Pfs//////++xO7/S5GA/ycg7P8nIOz2JyDscCcg7AEAAAAAAAAAAAAAAAAnIOwBJyDstScg7P8nIOz/Y8p5/2fHZf9Yv0z/YcF2/1rBUv8nIOz/JyDs/ycg7P8nIOy1JyDsAQAAAAAAAAAAJyDscCcg7P8nIOz/JyDs/4jQoP/p9+n//////05X3v9LkYD/JyDs/ycg7P8nIOz/JyDs/ycg7HAAAAAAJyDsJicg7PYnIOz/JyDs/zUu7f/+/v////////////89N+7/JyDs/yUo7f8nIOz/JyDs/ycg7P8nIOz2JyDsJicg7IAnIOz/JyDs/ycg7P9hXPH////////////t/P//GIr2/wfD+/8Gyfz/DKv5/yM57/8nIOz/JyDs/ycg7H8nIOyzJyDs/ycg7P8nIOz/jov1////////////Otz9/w3G/P8cWfH/JSvt/ycg7P8nIOz/JyDs/ycg7P8nIOyzJyDs5icg7P8nIOz/JyDs/7u5+f///////////27l/v8E0v3/BNL9/wTQ/f8Oofn/IT7v/ycg7P8nIOz/JyDs5icg7OYnIOz/JyDs/ycg7P/p6P3/uWsC////////////5fr//6Po/f8Thfb/DKv5/w6f+f8nIOz/JyDs/ycg7OYnIOyzJyDs/ycg7P8nIOz/9/b+/////////////////7lrAv/V1Pv/JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOyzJyDsgCcg7P8nIOz/JyDs/8/N+///////////////////////iIX1/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDsfycg7CYnIOz2JyDs/ycg7P9FP+7/q6n4/+7u/f/n5v3/fXn0/yoj7P8nIOz/JyDs/ycg7P8nIOz/JyDs9icg7CYAAAAAJyDscCcg7P8nIOz/wsD6/+no/f/Y1/z/eHTz/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs/ycg7HAAAAAAAAAAACcg7AEnIOy1JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs/ycg7LUnIOwBAAAAAAAAAAAAAAAAJyDsAScg7HAnIOz2JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs9icg7HAnIOwBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJyDsJicg7IAnIOyzJyDs5icg7OYnIOyzJyDsgCcg7CYAAAAAAAAAAAAAAAAAAAAA+B8AAPAPAADAAwAAwAMAAIABAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAACAAQAAwAMAAMADAADwDwAA+B8AAA==</os:Image>
<os:Url type="text/html" method="GET" template="http://duckduckgo.com/html?t=trisquel&amp;q={searchTerms}">
</os:Url>
</SearchPlugin>
EOF

cat << EOF > debian/search/duckduckgo-ssl.xml
<SearchPlugin xmlns="http://www.mozilla.org/2006/browser/search/" xmlns:os="http://a9.com/-/spec/opensearch/1.1/">
<os:ShortName>DuckDuckGo (SSL)</os:ShortName>
<os:Description>Search DuckDuckGo (SSL)</os:Description>
<os:InputEncoding>UTF-8</os:InputEncoding>
<os:Image width="16" height="16">data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAAAANcNAADXDQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJyDsJmlk8pf6+v3s/v7+++zr/fcnIOyzJyDsgCcg7CYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnIOwBJyDscCcg7PZttJ7/7Pfs//////++xO7/S5GA/ycg7P8nIOz2JyDscCcg7AEAAAAAAAAAAAAAAAAnIOwBJyDstScg7P8nIOz/Y8p5/2fHZf9Yv0z/YcF2/1rBUv8nIOz/JyDs/ycg7P8nIOy1JyDsAQAAAAAAAAAAJyDscCcg7P8nIOz/JyDs/4jQoP/p9+n//////05X3v9LkYD/JyDs/ycg7P8nIOz/JyDs/ycg7HAAAAAAJyDsJicg7PYnIOz/JyDs/zUu7f/+/v////////////89N+7/JyDs/yUo7f8nIOz/JyDs/ycg7P8nIOz2JyDsJicg7IAnIOz/JyDs/ycg7P9hXPH////////////t/P//GIr2/wfD+/8Gyfz/DKv5/yM57/8nIOz/JyDs/ycg7H8nIOyzJyDs/ycg7P8nIOz/jov1////////////Otz9/w3G/P8cWfH/JSvt/ycg7P8nIOz/JyDs/ycg7P8nIOyzJyDs5icg7P8nIOz/JyDs/7u5+f///////////27l/v8E0v3/BNL9/wTQ/f8Oofn/IT7v/ycg7P8nIOz/JyDs5icg7OYnIOz/JyDs/ycg7P/p6P3/uWsC////////////5fr//6Po/f8Thfb/DKv5/w6f+f8nIOz/JyDs/ycg7OYnIOyzJyDs/ycg7P8nIOz/9/b+/////////////////7lrAv/V1Pv/JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOyzJyDsgCcg7P8nIOz/JyDs/8/N+///////////////////////iIX1/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDsfycg7CYnIOz2JyDs/ycg7P9FP+7/q6n4/+7u/f/n5v3/fXn0/yoj7P8nIOz/JyDs/ycg7P8nIOz/JyDs9icg7CYAAAAAJyDscCcg7P8nIOz/wsD6/+no/f/Y1/z/eHTz/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs/ycg7HAAAAAAAAAAACcg7AEnIOy1JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs/ycg7LUnIOwBAAAAAAAAAAAAAAAAJyDsAScg7HAnIOz2JyDs/ycg7P8nIOz/JyDs/ycg7P8nIOz/JyDs9icg7HAnIOwBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJyDsJicg7IAnIOyzJyDs5icg7OYnIOyzJyDsgCcg7CYAAAAAAAAAAAAAAAAAAAAA+B8AAPAPAADAAwAAwAMAAIABAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAACAAQAAwAMAAMADAADwDwAA+B8AAA==</os:Image>
<os:Url type="text/html" method="GET" template="https://duckduckgo.com/html?t=trisquel&amp;q={searchTerms}">
</os:Url>
</SearchPlugin>
EOF

cat << EOF > debian/search/trisquel-packages.xml
<SearchPlugin xmlns="http://www.mozilla.org/2006/browser/search/" xmlns:os="http://a9.com/-/spec/opensearch/1.1/">
<os:ShortName>Trisquel Packages</os:ShortName>
<os:Description>Search packages.trisquel.info</os:Description>
<os:InputEncoding>UTF-8</os:InputEncoding>
<os:Image width="16" height="16">data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACi0lEQVQ4jY2T3UtTARiH92/0DxR0E10EZTeBF3VhBEqF9EEXUURUpn2AmohNS60oUrRMQ1D86ItKKz+atjmd25w5S82zlXPOqfNsc27nzOPOeboQJlakL/xuXnifi+d9Xx1bLEVZxTHhxzjsRZJXkn3dVob9gQhp+d1kFhs5rv9C6s1ufnjEjQDHhJ+bNXbOlJsprHfwyxdMAgrrHbzvcxONxXF7F6l47eRUaR+qqq4BWgwCO85+5KjeyMm7Jraf62DnhU5aDZMA3KodJByR0DSNmjdDCFML7L/Wy4IYQReNxUm52oXB7sEzG6Lb5sEx7uNgXi8pOT04JuYwO308bB3mrclNfbuTFoNAWoGJmLSCzusPcij3M6GlGJUvhzDYpymuH6Tvq5cDuX0cKzHjW1jCMxtk1DWPecRLZqkFfcPwmoNgOMrhgl5m5sJJu2UNVqZnRY7oLaSXWDlRZuFGjZ3LlVYOFpg5c6+fxVB0XeLjV07OP7LwosfFs7YxGjvGsI3Nkl5iJTW/nz3Zvey+1Mm+rA7y6uyEI9LGNWqahlOY59Ogh1HXPAA51Va2ne4gv86GazqQlPhn/fMOFCXB3qxOMu+YCASXud80RNFzC7Yx/9YAYijKrotd1LZP8OTtKG6viCSvcL16AGF6cXNATFohJbubqnffefruG+M/51FVlWaDQE3b+OYAgOyqQTKKTATEZT6YXQieAOXNIzR2Tf4N0DQNVVVZXV1FURQURcHrF8m4beTsgwFaewTKm0c4qjcy5RNJJBJJoTpN04jH40QiEUKhEKIoJuOemuFBs40rFWbuN9kYFzwEg0HC4TCSJK3/QiKRQJZlZFkmHo//N7IsI0kSiqKgaRq/AbKDgxgo7zYPAAAAAElFTkSuQmCC</os:Image>
<SearchForm>http://packages.trisquel.info/</SearchForm>
<os:Url type="text/html" method="GET" template="http://packages.trisquel.info/search?suite=default&amp;section=all&amp;arch=any&amp;searchon=names&amp;keywords={searchTerms}">
</os:Url>
</SearchPlugin>
EOF

cat << EOF > debian/search/trisquel.xml
<SearchPlugin xmlns="http://www.mozilla.org/2006/browser/search/" xmlns:os="http://a9.com/-/spec/opensearch/1.1/">
<os:ShortName>Trisquel</os:ShortName>
<os:Description>Trisquel GNU/Linux</os:Description>
<os:InputEncoding>UTF-8</os:InputEncoding>
<os:Image width="16" height="16">data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACi0lEQVQ4jY2T3UtTARiH92/0DxR0E10EZTeBF3VhBEqF9EEXUURUpn2AmohNS60oUrRMQ1D86ItKKz+atjmd25w5S82zlXPOqfNsc27nzOPOeboQJlakL/xuXnifi+d9Xx1bLEVZxTHhxzjsRZJXkn3dVob9gQhp+d1kFhs5rv9C6s1ufnjEjQDHhJ+bNXbOlJsprHfwyxdMAgrrHbzvcxONxXF7F6l47eRUaR+qqq4BWgwCO85+5KjeyMm7Jraf62DnhU5aDZMA3KodJByR0DSNmjdDCFML7L/Wy4IYQReNxUm52oXB7sEzG6Lb5sEx7uNgXi8pOT04JuYwO308bB3mrclNfbuTFoNAWoGJmLSCzusPcij3M6GlGJUvhzDYpymuH6Tvq5cDuX0cKzHjW1jCMxtk1DWPecRLZqkFfcPwmoNgOMrhgl5m5sJJu2UNVqZnRY7oLaSXWDlRZuFGjZ3LlVYOFpg5c6+fxVB0XeLjV07OP7LwosfFs7YxGjvGsI3Nkl5iJTW/nz3Zvey+1Mm+rA7y6uyEI9LGNWqahlOY59Ogh1HXPAA51Va2ne4gv86GazqQlPhn/fMOFCXB3qxOMu+YCASXud80RNFzC7Yx/9YAYijKrotd1LZP8OTtKG6viCSvcL16AGF6cXNATFohJbubqnffefruG+M/51FVlWaDQE3b+OYAgOyqQTKKTATEZT6YXQieAOXNIzR2Tf4N0DQNVVVZXV1FURQURcHrF8m4beTsgwFaewTKm0c4qjcy5RNJJBJJoTpN04jH40QiEUKhEKIoJuOemuFBs40rFWbuN9kYFzwEg0HC4TCSJK3/QiKRQJZlZFkmHo//N7IsI0kSiqKgaRq/AbKDgxgo7zYPAAAAAElFTkSuQmCC</os:Image>
<os:Url type="text/html" method="GET" template="http://trisquel.info/search/node/{searchTerms}?page={startPage}">
</os:Url><os:Url type="application/rss+xml" method="GET" template="http://trisquel.info/opensearch/node/{searchTerms}?page={startPage}">
</os:Url>
</SearchPlugin>
EOF
echo "debian/search/* /usr/lib/abrowser-addons/searchplugins" >> debian/abrowser.install.in

# Disable search field at extensions panel
#sed  '/header-search/d; /search.placeholder/d' -i toolkit/mozapps/extensions/content/extensions.xul
cat << EOF >> toolkit/mozapps/extensions/content/extensions.css
#header-search {
  display:none;
}
EOF

find -wholename '*/brand.dtd' |xargs /bin/sed 's/trademarkInfo.part1.*/trademarkInfo.part1 "">/' -i

for STRING in community.end3 community.exp.end community.start2 community.mozillaLink community.middle2 community.creditsLink community.end2 contribute.start contribute.getInvolvedLink contribute.end channel.description.start channel.description.end
do
 find -name aboutDialog.dtd | xargs sed -i "s/ENTITY $STRING.*/ENTITY $STRING \"\">/"
done

for STRING in rights.intro-point3-unbranded rights.intro-point4a-unbranded rights.intro-point4b-unbranded rights.intro-point4c-unbranded
do
 find -name aboutRights.dtd | xargs sed -i "s/ENTITY $STRING.*/ENTITY $STRING \"\">/"
done

replace www.mozilla.com/abrowser/central trisquel.info/browser
replace www.mozilla.com/legal/privacy trisquel.info/legal

sed -i 's/<a\ href\=\"http\:\/\/www.mozilla.org\/\">Mozilla\ Project<\/a>/<a\ href\=\"http\:\/\/www.trisquel.info\/\"\>Trisquel\ Project<\/a>/g' browser/base/content/overrides/app-license.html

# We went too far...
replace "Trisquel Public" "Mozilla Public" .
replace "Trisquel Foundation" "Mozilla Foundation" .
replace "Trisquel Corporation" "Mozilla Corporation" .
replace "abrowser.com" "firefox.com" .
#sed -i 's/iceweasel, abrowser, icecat,/iceweasel, firefox, icecat,/g' debian/control.in
sed '/Provides/s/abrowser-locale/firefox-locale/' -i debian/control.langpacks

# Restore useragent to Firefox
sed '/MOZILLA_UAVERSION/ s:Abrowser/:Firefox/:' -i netwerk/protocol/http/nsHttpHandler.cpp

# Set migrator scripts
sed 's/Abrowser/Firefox/g; s/abrowser/firefox/g' -i browser/components/migration/src/AbrowserProfileMigrator.js

# Postinst script to manage profile migration and system links
echo '

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] ; then

[ -f /usr/bin/firefox ] || ln -s /usr/bin/abrowser /usr/bin/firefox

for HOMEDIR in $(grep :/home/ /etc/passwd |grep -v usbmux |grep -v syslog|cut -d : -f 6)
do
    [ -d $HOMEDIR/.mozilla/abrowser ] && continue || true
    [ -d $HOMEDIR/.mozilla/firefox ] || continue
    echo Linking $HOMEDIR/.mozilla/firefox into $HOMEDIR/.mozilla/abrowser
    ln -s $HOMEDIR/.mozilla/firefox $HOMEDIR/.mozilla/abrowser
done 
fi
exit 0 ' >> debian/abrowser.postinst.in

debian/rules debian/control
touch -d "yesterday" debian/control
debian/rules debian/control

changelog  "Rebranded for Trisquel"

compile
