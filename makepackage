#!/bin/bash
#
#    Copyright (C) 2011  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

set -e

export DEBIAN_FRONTEND=noninteractive
MEM=$(cat /proc/meminfo |grep MemTotal| awk '{print $2}')
GPGKEY=8D8AEBF1

for VAR in LANG LANGUAGE LC_ALL LC_TIME LC_MONETARY LC_ADDRESS LC_TELEPHONE LC_MESSAGES LC_NAME LC_MEASUREMENT LC_IDENTIFICATION LC_IDENTIFICATION LC_NUMERIC LC_PAPER LANG
do
    unset $VAR
done
export LANG=C
export HOME=/root
export DATE=$(date +'%a, %d %b %Y %T %z')

cd $(dirname $0)

if [ $# -lt 1 ]
then
    echo "Usage: $0 package"
    echo example: $0 linux lucid
    exit 1
fi

PACKAGE=$1
CODENAME=$(grep 'export CODENAME=' helpers/config |sed 's/.*=//')
UPSTREAM=$(grep 'export UPSTREAM=' helpers/config |sed 's/.*=//')
WORKDIR=tmp/makepackage
REPOSITORY=$PWD/repos/$CODENAME
HELPERS=$PWD/helpers
for REPO in $(git branch|sed 's/.* //;/master/d')
do
[ -d $REPOSITORY ] && continue
mkdir -p repos/$REPO/conf repos/$REPO/incoming
cat << EOF > repos/$REPO/conf/distributions
Origin: Trisquel
Label: Trisquel
Suite: $REPO
Version: 42
Codename: $REPO
Architectures: i386 amd64 source
Components: main
UDebComponents: main
DebIndices: Packages Release . .gz .bz2
UDebIndices: Packages . .gz .bz2
DscIndices: Sources Release .gz .bz2
Log: $REPO.log
Description: Trisquel GNU/Linux packages for the $UPSTREAM based release
EOF

if gpg -K | grep -q $GPGKEY
then
    echo "SignWith: $GPGKEY" >> repos/$REPO/conf/distributions
fi

cd repos/$REPO
reprepro -v export
cd ../..
done

disablescripts(){
    # Disable service starter scripts
    for i in /usr/sbin/invoke-rc.d /sbin/start /sbin/start-stop-daemon
    do
        mv $CHROOT/$i $CHROOT/$i.real
        cp $CHROOT/bin/true $CHROOT/$i
    done
}

createjail () {
    [ -d jails ] || mkdir jails
    CHROOT=jails/$UPSTREAM-$JAILARCH
    C="chroot $CHROOT"
    MIRROR=http://mir1.ovh.net/mirrors/ftp.ubuntu.com/ubuntu

    mkdir $CHROOT-tmp
    mount -t tmpfs -o size=500M none $CHROOT-tmp
    debootstrap --arch=$JAILARCH $UPSTREAM $CHROOT-tmp $MIRROR
    fuser -k $CHROOT-tmp
    cp -a $CHROOT-tmp $CHROOT
    umount $CHROOT-tmp
    rm $CHROOT-tmp -rf
    echo "127.0.0.1 localhost" > $CHROOT/etc/hosts
    mount none $CHROOT/proc -t proc
    mount none $CHROOT/dev/pts -t devpts

    disablescripts

    if gpg -K | grep $GPGKEY -q
    then
        cp /root/.gnupg $CHROOT/root -a
    else
        echo GPG private key for $GPGKEY not found, the packages and repositories will not be signed.
    fi

    echo $UPSTREAM-$JAILARCH  > $CHROOT/etc/debian_chroot
cat << EOF > $CHROOT/etc/apt/sources.list
deb $MIRROR $UPSTREAM main universe
deb $MIRROR $UPSTREAM-updates main universe
deb $MIRROR $UPSTREAM-security main universe
deb $MIRROR $UPSTREAM-backports main universe
deb-src $MIRROR $UPSTREAM main universe
deb-src $MIRROR $UPSTREAM-updates main universe
deb-src $MIRROR $UPSTREAM-security main universe
deb-src $MIRROR $UPSTREAM-backports main universe
EOF

cat << EOF > $CHROOT/etc/apt/apt.conf.d/90recommends
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

    export DEBIAN_FRONTEND=noninteractive

    [ $UPSTREAM != hardy -a $UPSTREAM != lucid -a $UPSTREAM != maverick ] &&  echo "force-unsafe-io" > $CHROOT/etc/dpkg/dpkg.cfg.d/02apt-speedup
    $C apt-get update
    [ $UPSTREAM != hardy ] && $C apt-get --force-yes -y install eatmydata
    [ $UPSTREAM != hardy ] && $C apt-get --force-yes -y install sysv-rc dpkg sysvinit-utils upstart
    disablescripts
    $C apt-get --force-yes -y dist-upgrade
    $C apt-get --force-yes -y install devscripts build-essential liburi-perl python-setuptools pkgbinarymangler wget rpl aptitude quilt fakeroot ccache
    $C apt-get clean

    wget -o /dev/null -O $CHROOT/tmp/key.gpg http://archive.trisquel.info/trisquel/trisquel-archive-signkey.gpg 
    $C apt-key add /tmp/key.gpg

    # Hack for i386
    if [ $JAILARCH = "i386" ]
    then
        for BIN in /bin/uname /usr/bin/arch
        do
        [ -f $CHROOT/$BIN ] || continue
        mv $CHROOT/$BIN $CHROOT/$BIN.orig
cat << EOF > $CHROOT/$BIN
#!/bin/bash

if [ \$# -eq 0 ]
then   
        $BIN.orig | sed s/x86_64/i686/g
else   
        $BIN.orig "\$*" | sed s/x86_64/i686/g
fi
EOF
        chmod 755 $CHROOT/$BIN
done
    fi
umount $CHROOT/proc $CHROOT/dev/pts
}

prepare(){
[ -d jails/$UPSTREAM-$JAILARCH ] || createjail
cat << EOF > jails/$UPSTREAM-$JAILARCH/tmp/update
mount -t proc none /proc
export DEBIAN_FRONTEND=noninteractive
export LANG=C
apt-get update
apt-get --force-yes -y install sysv-rc dpkg sysvinit-utils upstart
for i in /usr/sbin/invoke-rc.d /sbin/start /sbin/start-stop-daemon
do
    mv \$i \$i.real
    cp /bin/true \$i
done
export DEBIAN_FRONTEND=noninteractive
apt-get --force-yes -y dist-upgrade
umount /proc
EOF
chroot jails/$UPSTREAM-$JAILARCH bash /tmp/update
if [ -f jails/$UPSTREAM-$JAILARCH/CurrentlyBuilding ]
then
    echo The $UPSTREAM-$JAILARCH jail appears to be running $(cat jails/$UPSTREAM-$JAILARCH/CurrentlyBuilding |grep Package: |sed 's/Package:\ //'), aborting.
    exit 1
fi
    CHROOT=jails/$UPSTREAM-$JAILARCH disablescripts
}

tmpumount(){
  grep jails/$UPSTREAM-$JAILARCH/tmp /proc/mounts -q || return 0
  umount jails/$UPSTREAM-$JAILARCH/tmp && return
  echo ERROR: could not umount tmpfs at jails/$UPSTREAM-$JAILARCH/tmp
  exit 1
}
tmpmount(){
  [ $MEM -lt 16000000 ] && return
  grep jails/$UPSTREAM-$JAILARCH/tmp /proc/mounts -q && tmpumount
  echo $PACKAGE | grep -q "linux" && return
  mount -t tmpfs -o size=15G none jails/$UPSTREAM-$JAILARCH/tmp
}

compile(){
    [ -d jails/$UPSTREAM-$JAILARCH/$WORKDIR ] && rm -rf jails/$UPSTREAM-$JAILARCH/$WORKDIR/
tmpmount
    cp -a helpers/ jails/$UPSTREAM-$JAILARCH/$WORKDIR/

cat << EOF > jails/$UPSTREAM-$JAILARCH/usr/local/sbin/makepackage-handler
#!/bin/bash
WORKDIR=$WORKDIR
PACKAGE=$PACKAGE
cd $WORKDIR
[ -d LOGS ] || mkdir LOGS
[ -d PACKAGES ] || mkdir PACKAGES
export LD_PRELOAD="${LD_PRELOAD:+$LD_PRELOAD:}/usr/lib/libeatmydata/libeatmydata.so"
export PATH="/usr/lib/ccache:${PATH}"
bash -e make-$PACKAGE 2>&1 || exit 1
EOF

cat << EOF > jails/$UPSTREAM-$JAILARCH/etc/pkgbinarymangler/striptranslations.conf
enable: true
components: main
invalid_currentlybuilding: ignore
posuffix: translations
oem_blacklist: partner
EOF

cat << EOF > jails/$UPSTREAM-$JAILARCH/etc/pkgbinarymangler/sanitychecks.conf
enable: true
EOF

cat << EOF > jails/$UPSTREAM-$JAILARCH/etc/pkgbinarymangler/maintainermangler.conf
enable: true
invalid_currentlybuilding: ignore
EOF

cat << EOF > jails/$UPSTREAM-$JAILARCH/etc/pkgbinarymangler/maintainermangler.overrides
default: Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
ignore_domains: trisquel.info sognus.com listas.trisquel.info gnu.org fsf.org
ignore_emails: ruben@trisquel.info
EOF

    chmod 755 jails/$UPSTREAM-$JAILARCH/usr/local/sbin/makepackage-handler

    if ! chroot jails/$UPSTREAM-$JAILARCH makepackage-handler /$WORKDIR/make-$PACKAGE
    then
        rm jails/$UPSTREAM-$JAILARCH/CurrentlyBuilding
        exit 1
    fi
    find jails/$UPSTREAM-$JAILARCH/$WORKDIR/PACKAGES/$PACKAGE/ -maxdepth 1 -type f -exec cp {} $REPOSITORY/incoming/ \;
tmpumount
}

 JAILARCH=i386 prepare
 JAILARCH=amd64 prepare
 JAILARCH=i386 compile
 JAILARCH=amd64 compile

cd $REPOSITORY

for dsc in incoming/*.dsc
do
    if gpg -K | grep $GPGKEY -q
    then
        debsign -k$GPGKEY $dsc
    else
        echo GPG private key for $GPGKEY not found, the packages and repositories will not be signed.
    fi
    reprepro -v -C main includedsc $CODENAME $dsc || reprepro -S admin -P optional -v -b . -C main includedsc $CODENAME $dsc
done

find incoming -name *.deb -exec reprepro -v -C main includedeb $CODENAME {} \;
find incoming -name *.udeb -exec reprepro -v -C main includeudeb $CODENAME {} \;

rm incoming/*
